import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import SingleDogView from '@/views/SingleDogView.vue'
import AddDogView from '@/views/AddDogView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/dog/:id',
      name: 'single-dog',
      component: SingleDogView
    },
    {
      path: '/add-dog',
      name: 'add-dog',
      component: AddDogView
    }
  ]
})

export default router
