import axios from "axios";
import type { Dog } from "./entities";



export async function fetchAllDogs() {
    const response = await axios.get<Dog[]>('http://localhost:8080/api/dog');
    return response.data;
}

export async function fetchOneDog(id:any) {
    const response = await axios.get<Dog>('http://localhost:8080/api/dog/'+id);
    return response.data;
}


export async function postDog(dog:Dog) {
    const response = await axios.post<Dog>('http://localhost:8080/api/dog', dog);
    return response.data;
}


export async function deleteDog(id:any) {
    await axios.delete<void>('http://localhost:8080/api/dog/'+id);
}


export async function updateDog(dog:Dog) {
    const response = await axios.put<Dog>('http://localhost:8080/api/dog/'+dog.id, dog);
    return response.data;
}