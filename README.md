# vue-spring
Projet vue dans lequel on fait des appels HTTP vers [une API Rest faite en Spring Boot](https://gitlab.com/simplonlyon/promo27/first-spring)

## Dépendances utilisées
* Vue-router pour créer des pages sur notre application vue
* Axios pour faire des appels http vers une API (techniquement on pourrait utiliser `fetch` qui existe par défaut dans JS, mais axios est un peu plus agréable à utiliser)
* Bootstrap pour le responsive et certains composants

## How To Use
1. Cloner le projet
2. `npm install`
3. `npm run dev`
4. Ouvrir et lancer le [projet spring boot](https://gitlab.com/simplonlyon/promo27/first-spring)
5. Accéder à l'application sur http://localhost:5173

## Concepts du projet
### Les requêtes http
Pour lier un projet frontend avec un projet backend sous forme d'API Rest, on devra faire en sorte de récupérer les données mise à disposition par le serveur via des requêtes HTTP aux moments où on en a besoin pour les afficher.

On peut créer des fichier séparés pour regrouper les différentes requêtes vers le serveur comme dans [ce fichier](src/dog-service.ts)

Ici on fait une requête en GET pour aller récupérer la liste des chiens sur notre API Rest, la plupart des requêtes HTTP ressembleront fortement à celle ci.
```ts
export async function fetchAllDogs() {
    const response = await axios.get<Dog[]>('http://localhost:8080/api/dog');
    return response.data;
}
```
Côté component/view, on utilise le onMounted pour faire en sorte de déclencher quelque chose au moment du lancement du component (ici la [page d'accueil](src/views/HomeView.vue)). On y appelle notre `fetchAllDogs` pour aller récupérer tous les chiens quand on arrive sur la page principale et on les stocks dans une ref qui sera affichée dans le template
```ts
onMounted(async () => {
  dogList.value = await fetchAllDogs();
});
```

### Route paramétrée
Dans le [router](src/router/index.ts) il est possible de créer des routes dites "paramétrées", c'est à dire qui auront une partie de l'url qui sera une variable (de la même manière que les routes de type `@GetMapping("/{id}")` côté Java Spring Boot)

On définit le paramètre de la route avec un `:nomParam`. La route ci-dessous s'activera donc si l'url ressemble à http://localhost:5173/dog/1 ou http://localhost:5173/dog/10 ou http://localhost:5173/dog/truc ou autre
```ts
    {
      path: '/dog/:id',
      name: 'single-dog',
      component: SingleDogView
    }
```

On peut ensuite récupéré la valeur du paramètre en question depuis un component en utilisant le `useRoute()` de vue-router. Le `.id` après le route.params correspond au nom qu'on a donné au paramètre dans notre route (ici `:id`)
```ts
const route = useRoute();
console.log(route.params.id);
```

La valeur de ce paramètre pourra être utilisé pour faire des requêtes http (par exemple `fetchOneDog(route.params.id)`). On peut le mettre dans un `onMounted` exactement comme dans le HomeView ou mieux le mettre dans un `watch` pour faire que la page se mette à jour si on passe d'un chien à un autre depuis la page du chien
```ts
watch(() => route.params.id, async () => {
  dog.value = await fetchOneDog(route.params.id);
}, {immediate: true});
```

### Event de component
Beaucoup de doc et de dév font le choix de ne pas laisser la responsabilité à leurs component de faire les appels vers le serveur. Par exemple, un component qui contient un formulaire, ce qu'on fait avec les données du formulaire une fois celui ci soumis ne serait pas géré par le component lui même mais par son parent par exemple.

L'intérêt est d'avoir des components plus paramétrables et génériques pouvant être utilisés dans différents contextes. Pour faire ça on utilise des event de component avec le defineEmits.

Ici par exemple on dit que notre component possède un event load et un event add
```ts
const emit = defineEmits(['load', 'add']);

//Ou bien avec cette syntaxe là qui permet d'indiquer le type de données que devra
//envoyer l'event, rien dans le cas du load et une Person dans le cas du add
const emit =  defineEmits<{
  load: [],
  add: [person:Person]
}>();
```

Ensuite on indique quelque part dans le component là où doivent se déclencher chaque event, selon la logique voulue. Par exemple là on peut dire que le load se lance dans le onMounted et que le add se lance quand on soumet un formulaire ou autre.

```ts
onMounted(() => {
  emit('load');
});

function handleSumit() {
  emit('add', person.value);
}
```

Pour faire quelque chose sur ces event, il faudra que dans le template du parent, celui ci surveille les events en question et leur assigne une fonction à exécuter quand ils seront déclenchés.

Ici, le parent exécutera la fonction doStuff() quand MonComponent sera chargé et déclenchera un postPerson quand le add sera declenché. À noter qu'on peut récupérer la valeur de l'event avec la variable `$event`
```html
<MonComponent @load="doStuff()" @add="postPerson($event)" />
```
## Exercices
### I. Faire l'entité et afficher les chiens
1. Dans le dossier src rajouter un fichier entities.ts et dedans créer une interface Dog avec un id optionnel en number, un name, une breed et une birthdate en string
2. Toujours dans le src, créer un fichier dog-service.ts et dedans faire une export async function fetchAllDogs, dans cette fonction utiliser axios pour faire un get<Dog[]> vers l'url http://localhost:8080/api/dog (on await) et faire un return du .data de la réponse
3. Dans le HomeView.vue, créer une variable dogs ref<Dog[]> avec un tableau vide dedans, et juste en dessous dans un onMounted assigner à dogs.value le résultat du fetchAllDogs avec un await. Pour voir si ça marche, afficher {{ dogs }} dans le template
4. Dans le dossier components, créer un DogCard.vue qui va faire un defineProps avec un dog:Dog dedans. Côté template faire une card bootstrap et dans la card mettre les infos du chien
5. Dans le HomeView rajouter un DogCard avec un v-for sur les dogs dessus et assigner l'item de la boucle au :dog de la DogCard 
6. Rajouter des div.row et des div.col autour de la DogCard (et du coup déplacer le v-for sur la col) pour faire en sorte d'afficher 3 Chiens par ligne en md, 2 en sm et 1 par défaut
### II. Faire le header mais mieux
1. Copier une navbar sur le site de bootstrap et la mettre dans le App.vue à la place de notre nav actuelle ou mieux, dans un component Header à part
2. Dans le nav de la navbar, remplacer les <a> par des RouterLink (bon pour l'instant on en a qu'un seul) avec la classe qui va bien
3. Dans le App toujours, créer une ref navOpen initialisé à false, et rajouter un event @click sur le button .navbar-toggler qui va faire que navOpen = !navOpen
4. Sur la div qui a la classe navbar-collapse, rajouter une classe conditionnelle avec :class qui ajoutera la classe 'show' si navOpen est true
### III. Un chien, une baballe, une page
1. Dans le dossier views, créer une nouvelle page SingleDogView.vue avec pour le moment juste un p'tit h1 d'exemple
2. Rajouter un nouveau lien dans le routeur pour pointer dessus, ça sera une route paramétrée /dog/:id
3. Dans le DogCard rajouter un RouterLink dans la card qui pointera sur le lien /dog/ concaténé avec l'id du chien qu'on a sous la main
4. Dans le dog-service, créer une nouvelle fonction fetchOneDog qui attendra un argument id:any et qui concaténera cette id au bout de l'url '..../api/dog/'  (changer aussi le typage du get pour faire qu'il renvoie un seul Dog plutôt qu'un tableau)
5. Dans le SingleDogView créer une ref<Dog> et récupérer l'état du router avec un useRoute
6. Dans le onMounted, faire un peu comme on a fait dans le HomeView mais utiliser le route.params.id pour aller récupérer le paramètre d'url id et le donner à la fonction fetchOneDog
7. Faire le template du chien
### IV. Formuchien
1. Créer une nouvelle page/view AddDog et l'assigner au lien /add-dog sur le router. Rajouter aussi un RouterLink dans le Header.vue pour cette page
2. Dans le dog-service, rajouter une fonction postDog(dog:Dog) qui va faire exactement comme le findAll mais qui fera un post cette fois, renverra un Dog et à qui on donnera l'argument dog après l'url (`post<Dog>('url', dog)`)
3. Créer un nouveau component DogForm.vue et faire un <form> dans le template avec 3 inputs (name,breed,birthdate)
4. Créer une ref<Dog> dans le script et assigner chaque propriétés du dog aux input avec des v-model
5. Rajouter un event au submit.prevent sur le formulaire qui déclenchera une fonction qui fera un appel à notre postDog en lui donnant à manger la ref dog qu'on a faite
**Bonus :** Utiliser le useRouter() pour rediriger vers la page du chien créé une fois le post terminé. Rajouter aussi de la validation sur les inputs
### V. Supprechien
1. Dans le dog-service, rajouter une fonction deleteDog en vous inspirant du fetchOneDog, sauf que cette fois, on fait un .delete<void> (et du coup même pas besoin de return en vrai)
2. Dans le SingleDogView.vue, rajouter un bouton Delete dans le template et le lier au click avec une fonction async handleDelete()
3. Dans cette fonction, lancer le deleteDog en lui donnant l'id du chien en argument puis utiliser le useRouter pour rediriger vers la page d'accueil une fois le chien supprimé
4. Dans le DogCard, rajouter aussi un bouton Delete et le lier à un handleDelete aussi (mais pas async cette fois)
5. Rajouter un defineEmits dans la DogCard avec un event qu'on va appeler delete (et qui éventuellement attend un Dog en argument) et faire en sorte de emit cet event dans le handleDelete
6. Côté HomeView, créer une fonction async remove(dog:Dog) dans le script et l'assigner au @delete de notre DogCard
7. Dans cette fonction remove, appeler le deleteDog du service puis faire un filter sur le dogList.value pour retirer le chien qui a été supprimé
### VI. Mettre à jour le chien
1. Dans le SingleDogView, rajouter une nouvelle ref boolean editing initialisée à false
2. Dans le template, avec des v-if, faire que si editing est true, on affiche le DogForm, sinon on affiche le chien comme on le faisait jusqu'à maintenant
3. Rajouter un bouton/une icône/autre qui passera editing de true à false ou de false à true quand on click dessus
4. Dans le DogForm rajouter une props initialDog?:Dog (optionnelle donc), la récupérer dans une variable (comme fait dans le DogCard) et en dessous de la ref dog, rajouter une ligne avec `Object.assign(dog.value, initialDog)` (c'est une manière pour assigner les valeurs d'un objet à un autre)
5. Côté SingleDogView, mettre à jour le template pour donner le chien au DogForm (ça devrait pré-remplir le form)
6. Dans le dog-service, créer un updateDog(dog:Dog) qui va taper sur /api/dog/id en put et qui enverra le dog en body
7. Deux choix possibles maintenant : Soit faire une condition dans le DogForm qui déclenche soit le updateDog soit le postDog selon si on a un quelque chose dans initialDog ou pas. Soit modifier le DogForm en rajoutant un event (comme dans le bouton delete de la DogCard) pour faire que ça soit le parent qui décide quoi faire lorsqu'on submit le form (donc dans AddDogView, faire un postDog et dans SingleDogView, faire un updateDog)